import React, { useEffect, useState } from "react";
import { ethers} from "ethers";
import alertify from "alertifyjs";
import 'alertifyjs/build/css/alertify.css';
import './app.css';
import SVGWordNFTs from './SVGWordNFTs.json';

// eslint-disable-next-line  @typescript-eslint/no-explicit-any
declare const window: any

const CONTRACT_ADDRESS = "0x04BFf901E21db5aF22025DF43808655D07b28485";
const OPENSEA_LINK = "https://testnets.opensea.io/collection/choppyseas-l0r76dzmjl";
const FAUCET_LINK = "https://rinkebyfaucet.com/";
const RINKEBY_CHAIN_ID = "0x4";
const NFT_COUNT_LIMIT = 1000;

alertify.set('notifier', 'position', 'top-right');

const App: React.FC = () => {
  const [currentAccount, setCurrentAccount] = useState("");
  const [transactionLink, setTransactionLink] = useState("");
  const [nftLink, setNftLink] = useState("");
  const [currentlyMinting, setCurrentlyMinting] = useState(false);
  const [mintCount, setMintCount] = useState(0);
  const [mintFull, setMintFull] = useState(false);

  const checkMetaMask = async () => {
    // Check first if the browser does not have the metamask extension.
    if (window.ethereum == null) {
      alertify.error("No ethereum obect detected - make sure you have metamask");
    }
  };

  const checkNetwork = async () => {
    // Ensure the user is connected to Rinkeby, the only supported chain.
    const chainId = await window.ethereum.request({ method: 'eth_chainId' });
    if (chainId !== RINKEBY_CHAIN_ID) {
      alertify.error("You are not connected to the Rinkeby Test Network - please fix in MetaMask");
    }
  };

  const checkWallet = async () => {
    // Connect to the first account in their metamask.
    const accounts = await window.ethereum.request({ method: 'eth_accounts' });
    if (accounts.length !== 0) {
      setCurrentAccount(accounts[0]);
    }
  };

  const connectWallet = async () => {
    // Prompts metamask to connect the first account.
    const accounts = await window.ethereum.request({ method: "eth_requestAccounts" });
    setCurrentAccount(accounts[0]);
  };

  const getMintCount = async () => {
    // Calls the contract to get the total number of NFTs that have been minted.
    const provider = new ethers.providers.Web3Provider(window.ethereum);
    const signer = provider.getSigner();
    const connectedContract = new ethers.Contract(CONTRACT_ADDRESS, SVGWordNFTs.abi, signer);
    const mintCount = await connectedContract.getTotalNFTsMintedSoFar();
    setMintCount(mintCount.toNumber());
    if (mintCount >= NFT_COUNT_LIMIT) {
      setMintFull(true);
    }
  };

  const setupMintEventListener = async () => {
    // Waits until the contract emits confirmation that an NFT was minted and
    // then sends the OpenSea link to the UI.
    const provider = new ethers.providers.Web3Provider(window.ethereum);
    const signer = provider.getSigner();
    const connectedContract = new ethers.Contract(CONTRACT_ADDRESS, SVGWordNFTs.abi, signer);
    connectedContract.on("NewSVGWordNFTMinted", (from, tokenId) => {
      const nftUrl = "https://testnets.opensea.io/assets/" + CONTRACT_ADDRESS + "/" + tokenId.toNumber();
      setNftLink(nftUrl);
      setCurrentlyMinting(false);
      const msg = alertify.success("Your NFT has been minted and sent to your wallet. It may be blank right now; it can take a couple minutes to render on OpenSea.")
      msg.delay(10);
    });
  };

  const mintNft = async () => {
    // Calls the contract to mint an NFT with the current account as signer.
    const provider = new ethers.providers.Web3Provider(window.ethereum);
    const signer = provider.getSigner();
    const connectedContract = new ethers.Contract(CONTRACT_ADDRESS, SVGWordNFTs.abi, signer);
    const nftTxn = await connectedContract.mintAnSVGWordNFT({ gasLimit: 10000000 });
    const txnUrl = "https://rinkeby.etherscan.io/tx/" + nftTxn.hash;
    setTransactionLink(txnUrl);
    setCurrentlyMinting(true);
    await nftTxn.wait();
  };

  // Check metamask and chain on first load only.
  useEffect(() => {
    checkMetaMask();
    checkNetwork();
    setupMintEventListener();
  }, []);

  // Check the wallet connection and account on every render
  useEffect(() => {
    checkWallet();
    getMintCount();
  });

  return (
    <div className="App">
      <div className="container">
        <div className="header-container">
          <p className="header gradient-text">(Adj)(Race)(Name)</p>
          {!currentAccount
            ? <button onClick={connectWallet} className="primary-info green-animation cursor-pointer">Connect Wallet</button>
            : <button className="primary-info green-animation">Connected: {currentAccount}</button>
          }
          <p />
          {currentAccount &&
            <div><p className="white-text">NFTs that have been minted so far:</p><p className="white-text">{mintCount} / {NFT_COUNT_LIMIT}</p></div>
          }
          {currentAccount && !currentlyMinting && !mintFull &&
            <button onClick={mintNft} className="primary-info purple-animation cursor-pointer">Mint NFT</button>
          }
          {transactionLink &&
            <p className="white-text"><a href={transactionLink} target="_blank" rel="noreferrer noopener">Your transaction on etherscan</a></p>
          }
          {(nftLink || currentlyMinting) &&
            (currentlyMinting
              ? <div id="load"><div>G</div><div>N</div><div>I</div><div>T</div><div>N</div><div>I</div><div>M</div></div>
              : <p className="white-text"><a href={nftLink} target="_blank" rel="noreferrer noopener">Your NFT on OpenSea</a></p>
            )
          }
        </div>
        <div className="footer-container">
          <p className="white-text"><a href={OPENSEA_LINK} target="_blank" rel="noreferrer noopener">See collection on OpenSea</a></p>
          <p className="white-text">No eth on Rinkeby? <a href={FAUCET_LINK} target="_blank" rel="noreferrer noopener">Use this free faucet</a></p>
        </div>
      </div>
    </div>
  );
};

export default App;